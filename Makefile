# Setup pipenv virtual env
install:
	python3 -m pipenv install --dev --skip-lock

# Build docker
build:
	docker build . --tag=simplepy:latest

run:
	docker run -it -p 80:8000 --name simplepy simplepy:latest

run-detached:
	docker run -itd -p 80:8000 --name simplepy simplepy:latest
	
stop:
	docker stop simplepy
# Invoke Python repl in venv
repl:
	python3 -m pipenv shell

# Run all automated tests (unit, integration, etc)
#tests:
#	python3 -m pipenv run pytest

# Run code through formatters
format: 
	python3 -m pipenv run isort --recursive .
	python3 -m pipenv run black simplepy

# Lint all source code
lint:
	python3 -m pipenv run pylint simplepy

# Lint but only emit errors (for automated builds)
lint-prod:
	python3 -m pipenv run pylint --rcfile=.pylintrc --errors-only simplepy 

# Typical (and default) clean that tries to avoid removing user created data that is gitignored
clean:
	git clean -xd --force --exclude .vscode

# Clean everything that is unversioned or gitignored
nuke:
	git clean -xd --force


# Useful standin for the build process that tries to be similar for dev purposes
validate: check lint-prod tests

# Check attributes of the project
check:
	# Confirm all imports are organized correctly
	python3 -m pipenv run isort --recursive --check-only .

	# Confirm no formatting changes are required
	python3 -m pipenv run black --check crosswind fixer_suites

