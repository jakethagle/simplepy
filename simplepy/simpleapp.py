"""A simple flask application for testing all things CI/CD"""
import optparse
import sys
import time

from flask import Flask

APP = Flask(__name__)

start = int(round(time.time()))


@APP.route("/")
def hello_world():
    """Entry point function to the webapp"""

    return "Hello world from simplepy!"


if __name__ == "__main__":
    parser = optparse.OptionParser(usage="python simpleapp.py -p ")
    parser.add_option(
        "-p", "--port", action="store", dest="port", help="The port to listen on."
    )
    (args, _) = parser.parse_args()
    if not args.port:
        print("Missing required argument: -p/--port")
        sys.exit(1)
    APP.run(host="0.0.0.0", port=int(args.port), debug=False)
